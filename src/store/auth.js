import axios from 'axios';
import { errorParser } from '../helpers';

export default {
  namespaced: true,
  state: {
    user: null,
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    getUser({ commit }) {
      return axios.get('api/v1/user/')
        .then((resp) => {
          if (resp.data.id === null) commit('setUser', false);
          else commit('setUser', resp.data);
        })
        .catch((err) => {
          if (err.response && err.response.status === 403) {
            commit('setUser', false);
            return false;
          }
          return Promise.reject(errorParser(err));
        });
    },
    login({ commit }, data) {
      return axios.post('api/v1/user/', data)
        .then(resp => commit('setUser', resp.data))
        .catch((err) => {
          if (err.response && [400, 401, 403].includes(err.response.status)) {
            commit('setUser', null);
          }
          return Promise.reject(errorParser(err));
        });
    },
    logout({ commit }) {
      return axios.delete('api/v1/user/')
        .then(() => commit('setUser', false))
        .catch(err => Promise.reject(errorParser(err)));
    },
  },
  getters: {
    hasAuthInfo: state => state.user !== null,
    isLoggedOn: (state, getters) => getters.hasAuthInfo && state.user !== false,
  },
};
