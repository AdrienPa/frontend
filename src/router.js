import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/Login.vue';
import Logout from './views/Logout.vue';
import Station from './views/Station.vue';
import NotFound from './views/NotFound.vue';
import store from './store';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: {
        name: 'station',
      },
    },
    {
      path: '/station',
      name: 'station',
      component: Station,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
    },
    {
      path: '*',
      component: NotFound,
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let authPromise = Promise.resolve();
    if (!store.getters['auth/hasAuthInfo']) authPromise = store.dispatch('auth/getUser');
    authPromise.then(() => {
      if (store.getters['auth/isLoggedOn']) next();
      else next({ name: 'login', query: { next: to.fullPath } });
    });
  } else next();
});

export default router;
