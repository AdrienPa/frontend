export const BASE_URL = process.env.VUE_APP_BASE_URL || 'http://localhost:8000/';
export const CSRF_HEADER = process.env.VUE_APP_CSRF_HEADER || 'X-CSRFToken';
export const CSRF_COOKIE = process.env.VUE_APP_CSRF_COOKIE || 'csrftoken';

export const MEASUREMENT_REFRESH_RATE = parseInt(process.env.VUE_APP_MEASUREMENT_REFRESH_RATE || '10000', 10);

export const ENERGY_SOURCES = {
  solar: {
    icon: 'sun',
    name: 'Panneau solaire',
    color: '#ffd000',
    ratio: 2000.0,
  },
  wind: {
    icon: 'wind',
    name: 'Éolienne',
    color: '#09f',
    ratio: 2000.0,
  },
  hydro: {
    icon: 'droplet',
    name: 'Hydrolienne',
    color: '#039',
    ratio: 2000.0,
  },
};

export const EQUIVALENTS = [
  {
    text: 'minutes pour recharger son smartphone',
    icon: 'smartphone',
    ratio: value => 750 / value,
  },
  {
    text: 'litres de gazole par jour',
    icon: 'fuel',
    ratio: value => value / 4000 * 24,
  },
  {
    text: 'grammes de charbon par heure',
    icon: 'fuel',
    ratio: value => value / 8.141,
  },
  {
    text: 'kilogrammes de TNT par heure',
    icon: 'fuel',
    ratio: value => value / 1.162,
  },
  {
    text: "minutes pour faire bouillir 1 litre d'eau",
    icon: 'droplet',
    ratio: value => value / 1.55,
  },
];
